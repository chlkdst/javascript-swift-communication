# README #

This project is a PoC to prove that we can actually let the WebView and Swift code communicate with each other.

### What is this repository for? ###

* iOS COP app

### Requirements ###
* Web Server for Chrome
* Xcode 8 with Swift 3
* iOS Simulator or iPhone / iPad device

### How do I get set up? ###

* Install [Web Server for Chrome](https://chrome.google.com/webstore/detail/web-server-for-chrome/ofhbbkphhbklhfoeikjpcbhemlocgigb?hl=en).
* Download snippets in https://bitbucket.org/snippets/jlundang/7o744
* Open Web Server for Chrome and simply choose the directory of snippet. Start!
* http://127.0.0.1:8887/index.html should now be accessible in any web browsers
* Open and run the Xcode project
* Click `Record Audio` button loaded in WebView, there should be JSON string message in console
* Click `Show context menu items`, the WebView should log in Safari Web Inspector debug console

### Who do I talk to? ###

* <mobaltazar@cambridge.org>
* <jlundang@cambridge.org>
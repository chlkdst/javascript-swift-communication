//
//  ViewController.swift
//  COP-JavaScriptCore
//
//  Created by jlundang on 13/09/2016.
//  Copyright © 2016 Cambridge University Press. All rights reserved.
//

import UIKit
import WebKit
// Reference: [WKWeb​View](http://nshipster.com/wkwebkit/)
class ViewController: UIViewController {
    @IBOutlet weak var webViewContainer: UIView!
    @IBOutlet weak var btnChangeBackground: UIButton!
    fileprivate var webView: WKWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // MARK: Injecting Behavior with User Scripts
        // `WKUserScript` allows JavaScript behavior to be injected at the start or end of document load. This powerful feature allows for web content to be manipulated in a safe and consistent way acros page requests.
        let source = "document.body.style.background = \"#D24D57\"" // User script can be injected to change the background color of a web page
        // `WKUserScript` objects are initialized with JavaSript source, as well as whether it should be injected at the start or end of loading page, and whether this behavior should be used for all frames or just the main frame.
        let userScript = WKUserScript(source: source, injectionTime: .atDocumentEnd, forMainFrameOnly: true)
        
        let userContentController = WKUserContentController()
        // The user script is then added to `WKUserContentController`, which is set on the `WKWebViewConfiguration` object passed in to the initializer of WKWebView.
        userContentController.addUserScript(userScript)
        
        // MARK: Message Handlers
        // Communcation from web to app has improved significantly as well, with message handlers. Just like `console.log` prints out information to the [Safari Web Inspector](https://developer.apple.com/library/content/documentation/AppleApplications/Conceptual/Safari_Developer_Guide/Introduction/Introduction.html) debug console, information from a web page can be passed back to the app by invoking:
        // window.webkit.messageHandlers.{NAME}.postMessage()
        // Add User Scripts to create hooks for webpage events that use Message Handlers to communicate status back to the app.
        userContentController.add(NotificationScriptMessageHandler(), name: "recordAudio")
        
        let configuration = WKWebViewConfiguration()
        configuration.userContentController = userContentController
        
        webView = WKWebView(frame: webViewContainer.bounds, configuration: configuration)
        webView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        webViewContainer.addSubview(webView)
        
        webView.load(URLRequest(url: URL(string: "http://127.0.0.1:8887/index.html")!))
    }
    
    @IBAction func changeBackgroundColor(_ sender: UIButton) {
        /// MARK: Swift communication → JavaScript
        webView.evaluateJavaScript("showContextMenuItems()", completionHandler: nil)
    }
}

// The name of the handler is configured in `addScriptMessageHandler()`, which registers a handler conforming to the `WKScriptMessageHandler` protocol:
class NotificationScriptMessageHandler: NSObject, WKScriptMessageHandler {
    func userContentController(_ userContentController: WKUserContentController, didReceive message: WKScriptMessage) {
        // TODO: React native codes based on JavaScript function called in web view
        print(message.body)
    }
}
